From chapter 12.2 "Reactive Web Applications Using Spring WebFlux" of the book "Beginning Spring Boot 2" Apress 2017

Using embedded MongoDB with `de.flapdoodle.embed.mongo`

Even though you use WebFlux reactive support at the controller layer, if you use a blocking API for 
data access like JDBC or JPA, your application will **not be fully reactive**. As of now, relational database 
vendors haven’t provided non-blocking driver implementations. There are some NoSQL datastores such as 
MongoDB, Cassandra, and Redis that provide reactive drivers.