package com.ajh.springdemo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import reactor.core.publisher.Mono;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WebFluxWithMongoDemoApplicationTests {
	@Autowired
	private WebTestClient webClient;

	@Test
	public void contextLoads() {
	}

	@Test
	public void listAllUsers() {
		webClient.get().uri("/api/users").accept(MediaType.APPLICATION_JSON_UTF8).exchange()
			.expectStatus().isOk()
			.expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
			.expectBodyList(User.class)
			.consumeWith(result -> assertEquals(2, result.getResponseBody().size()));
	}

	@Test
	public void createAndRetrieveUser() {
		User user = new User("Zinx", "zinx@gmail.com");
		String id = UUID.randomUUID().toString();
		user.setId(id);

		// Create user
		webClient.post().uri("/api/users").body(Mono.just(user), User.class).exchange()
			.expectStatus().isOk()
			.expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
			.expectBody(User.class)
			.consumeWith(result -> assertThat(result.getResponseBody()).isEqualToComparingFieldByField(user));

		// Retrieve user
		webClient.get().uri("/api/users/"+id).exchange()
			.expectStatus().isOk()
			.expectBody()
			.jsonPath("$.id").isEqualTo(id)
			.jsonPath("$.name").isEqualTo(user.getName())
			.jsonPath("$.email").isEqualTo(user.getEmail());
	}
}

