package com.ajh.springdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/users")
public class UserController {
	@Autowired
	private UserReactiveRepository userRepo;

	@GetMapping
	public Flux<User> allUsers() {
		return userRepo.findAll();
	}

	@GetMapping("/{id}")
	public Mono<User> findUserById(@PathVariable String id) {
		return userRepo.findById(id);
	}

	@PostMapping
	public Mono<User> saveUser(@RequestBody Mono<User> userMono) {
		return userMono.flatMap(user -> userRepo.save(user));
	}

	@DeleteMapping("/{id}")
	public Mono<Void> deleteUser(@PathVariable String id) {
		return userRepo.deleteById(id);
	}
}
