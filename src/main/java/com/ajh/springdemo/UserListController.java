package com.ajh.springdemo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.thymeleaf.spring5.context.webflux.ReactiveDataDriverContextVariable;

import reactor.core.publisher.Flux;

@Controller
public class UserListController {
	@Autowired
	private UserReactiveRepository userRepo;

	@GetMapping("/list-users")
	public String listUsers(Model model) {
		Flux<User> userFlux = userRepo.findAll();

		// Error:
		//   block()/blockFirst()/blockLast() are blocking, which is not supported in thread reactor-http-epoll-2
		List<User> userList = userFlux.collectList().block();

		model.addAttribute("users", userList);

		return "users";
	}

	@GetMapping("/list-users-chunked")
	public String listUsersChunked(Model model) {
		Flux<User> userFlux = userRepo.findAll();
		model.addAttribute("users", userFlux);

		return "users";
	}

	@GetMapping("/list-users-reactive")
	public String listUsersReactive(Model model) {
		Flux<User> userFlux = userRepo.findAll();
		model.addAttribute("users", new ReactiveDataDriverContextVariable(userFlux, 1000));

		return "users";
	}
}
